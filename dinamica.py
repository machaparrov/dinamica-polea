from turtle import Screen
import pygame



pygame.init()

screen = pygame.display.set_mode([500, 500])

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill((255, 255, 255))
    pygame.draw.polygon(screen, (0, 0, 0), ((350,400),(350,200),(100,400)))
    pygame.draw.circle(screen, (0, 0, 250) , (360,180), 25)
    pygame.draw.line(screen, (159, 213, 209)  , (250,242),(334,180), 5)
    pygame.draw.line(screen, (159, 213, 209)  , (370,202),(370,270), 5)
    pygame.draw.polygon(screen, (255, 0, 0), ((170,270),(240,217),(270,260),(202,314)))
    pygame.draw.polygon(screen, (255, 0, 0), ((355,270),(400,270),(400,350),(355,350)))
    pygame.display.flip()

pygame.quit()